# tor-2e-compendium-it
TOR2E - Compendio non ufficiale per il sistema tor2e - Versione italiana

## Compatibilità
Foundry VTT v11
            v12

## Installazione
Installa dalla configurazione FoundryVTT/moduli aggiuntivi
- Cerca TOR2E per trovare il modulo
O
- Installa il modulo con l'URL manifest : https://gitlab.com/ghorin/tor-2e-compendium-it/-/raw/main/module.json


## Descrizione
Questo modulo installa un compendio che contiene molti elementi utili per creare personaggi (PC, NPC, avversari) nel sistema The One Ring 2e (https://foundryvtt.com/packages/tor2e):
- creazione di Segni Distintivi, Virtù, Difetti, Abilità Cadute, Abilità e Premi
- creazione di armi, armature e scudi
- creazione di avversari da 2a edizione

Tutti questi elementi vengono creati
- con le loro statistiche
- senza alcun testo o immagine dai libri ufficiali
- con un riferimento dove trovare il testo e le immagini nei libri

Tutte le immagini utilizzate per gli articoli sono immagini fornite
- tramite sistema TOR2E => Caratteristiche distintive, Virtù, Difetti, Abilità Cadute e Ricompense
- oppure da Foundry VTT (nella cartella di installazione di Foundry VTT, nella sottocartella resources/app/public/icons) => armi, armature, scudi

Nota: non è implementato alcun effetto attivo in nessun elemento di questo compendio.


## Licenza
L'Unico Anello, La Terra di Mezzo e Il Signore degli Anelli e i personaggi, gli oggetti, gli eventi e i luoghi in essi contenuti sono marchi o marchi registrati di Saul Zaentz Company d/b/a Middle-earth Enterprises (SZC) e sono utilizzati su licenza di Sophisticated Games Ltd. e dei rispettivi licenziatari. Tutti i diritti riservati.

## Pronto all'uso / Pronto per la modifica
Come da diritto di proprietà e licenza, e poiché questo modulo compendio è pubblico: non contiene descrizioni di testo, né testo di regole, né immagini tratte da alcun libro di L'Unico Anello. Ma tutti gli oggetti, dopo essere stati creati in un gioco Loremaster Foundry VTT, sono in modalità privacy/non condivisi con il pubblico, e quindi il Loremaster può modificarli per aggiungere qualsiasi descrizione e immagine necessaria dalla propria copia principale del libro.


## Come funziona
Vai alla scheda "Compendio". Troverai i seguenti pacchetti:
     - tor-2e-compendio-it : Attrezzatura
     - tor-2e-compendio-it : Caratteristiche
     - tor-2e-compendio-it : Avversari
Puoi farlo
     - apri un pacchetto e trascina i contenuti nella scheda destra (Oggetti per Caratteristiche ed Equipaggiamento, Attori per Avversari)
     - oppure fai un clic destro sui pacchetti e importa tutti i contenuti nel tuo mondo
Documentazione Foundry VTT sui Compendi:  https://foundryvtt.com/article/compendium/